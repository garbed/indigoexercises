/*
 *      File: LargestProductInput.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
package com.erick.mx.model;

/**
 * Model for largest product input.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestProductInput {

    /**
     * The digit input (N).
     */
    private String digitInput;

    /**
     * The digit limit (K).
     */
    private int digitLimit;

    /**
     * Create a new instance of {@code LargestProductInput}.
     *
     * @param digitInput The digit input (N).
     * @param digitLimit The digit limit (K).
     */
    public LargestProductInput(final String digitInput, final int digitLimit) {
        this.digitInput = digitInput;
        this.digitLimit = digitLimit;
    }

    /**
     * Getter for digit input.
     *
     * @return The digit input (N).
     */
    public String getDigitInput() {
        return digitInput;
    }

    /**
     * Setter for the digit input (N).
     *
     * @param digitInput The digit input (N).
     */
    public void setDigitInput(final String digitInput) {
        this.digitInput = digitInput;
    }

    /**
     * Getter for the digit limit (K).
     *
     * @return The digit limit (K).
     */
    public int getDigitLimit() {
        return digitLimit;
    }

    /**
     * Setter for The digit limit (K).
     *
     * @param digitLimit The digit limit (K).
     */
    public void setDigitLimit(final int digitLimit) {
        this.digitLimit = digitLimit;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final LargestProductInput that = (LargestProductInput) o;

        if (digitLimit != that.digitLimit) {
            return false;
        }
        return digitInput != null ? digitInput.equals(that.digitInput) : that.digitInput == null;
    }

    @Override
    public int hashCode() {
        int result = digitInput != null ? digitInput.hashCode() : 0;
        result = 31 * result + digitLimit;
        return result;
    }

    @Override
    public String toString() {
        return "LargestProductInput{digitInput='" + digitInput + ", digitLimit=" + digitLimit + '}';
    }
}
