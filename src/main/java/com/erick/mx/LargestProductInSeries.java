/*
 *      File: LargestProductInSeries.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
package com.erick.mx;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.erick.mx.model.LargestProductInput;
import com.erick.mx.utility.UtilExercises;

/**
 * The largest product in series exercise.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestProductInSeries {

    /** Class logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LargestProductInSeries.class);

    /**
     * split value.
     */
    private static final String SPLIT_VALUE = " ";

    /**
     * Minimum value of range.
     */
    private static final int RANGE_MIN_VALUE = 1;

    /**
     * Maximum value of range.
     */
    private static final int RANGE_MAX_VALUE = 100;


    public static void main(final String[] args) {
        productExecution();
    }


    /**
     * Method to execute the product process.
     */
    public static void productExecution() {
        final ArrayList<LargestProductInput> valuesList = new ArrayList<>();
        int testCases = UtilExercises.validateRange(RANGE_MIN_VALUE, RANGE_MAX_VALUE, Integer.parseInt(UtilExercises.requestEntryValue()));

        int digits;
        int fraction;
        while (testCases > 0) {
            final Scanner scanner = new Scanner(System.in);

            final String[] parts = scanner.nextLine().split(SPLIT_VALUE);
            try {
                digits = Integer.parseInt(parts[0]);
                fraction = Integer.parseInt(parts[1]);
            } catch (final NumberFormatException e) {
                LOGGER.error(String.format("You can only enter numeric values, Cause: %s",e));
                throw e;
            }
            final String inputValue = UtilExercises.validateProductInput(digits);
            final LargestProductInput input = new LargestProductInput(inputValue, fraction);
            valuesList.add(input);
            testCases--;
        }

        for (LargestProductInput iterator : valuesList) {
            System.out.println(findLargesProduct(iterator.getDigitInput(), iterator.getDigitLimit()));
        }


    }

    /**
     * Method for find the largest product.
     *
     * @param input The input value.
     * @param limit The limit (K).
     * @return The largest product defined by the limit.
     */
    public static String findLargesProduct(final String input, final int limit) {
        final ArrayList<Integer> integers = new ArrayList<>();

        for (int i = 0; i < input.length() - limit; i++) {
            int var = 1;
            for (int j = i; j < i + limit; j++) {
                var *= Double.parseDouble(String.valueOf(input.charAt(j)));
            }
            integers.add(var);
        }
        return Collections.max(integers).toString();
    }
}
