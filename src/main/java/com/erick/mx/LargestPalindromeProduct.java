/*
 *      File: LargestPalindromeProduct.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
package com.erick.mx;

import java.util.ArrayList;

import com.erick.mx.utility.UtilExercises;

/**
 * The largest palindrome product.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestPalindromeProduct {

    /**
     * Minimum value of test cases.
     */
    private static final int MINIMUM_VALUE_TEST_CASE = 1;

    /**
     * Maximun value of test cases.
     */
    private static final int MAXIMUM_VALUE_TEST_CASE = 100;

    /**
     * Input minimum value.
     */
    private static final int INPUT_MINIMUM_VALUE = 101101;

    /**
     * Input maximum value.
     */
    private static final int INPUT_MAXIMUM_VALUE = 1000000;


    public static void main(final String[] args) {
        palindromeExecution();
    }


    /**
     * Method to validate if a number is palindrome.
     *
     * @param number Number to validate.
     * @return True if the value is palindrome, false otherwise.
     */
    public static boolean isPalindrome(final long number) {
        final String numberToEvaluate = String.valueOf(number);
        return new StringBuilder(numberToEvaluate).reverse().toString().equals(numberToEvaluate);
    }


    /**
     * Method for the largest palindrome which is a multiple of two 3-digit numbers.
     *
     * @param value Maximum value.
     * @return The largest palindrome or -1 if no such palindrome exists.
     */
    public static long findLargestPalindrome(final long value) {
        long palindrome = -1;

        for (int i = 999; i > 99; i--) {
            for (int j = i; j > 99; j--) {

                if (isPalindrome(i * j) && (i * j > palindrome) && (i * j <= value)) {
                    palindrome = i * j;
                }
            }
        }
        return palindrome;
    }

    /**
     * Method to execute the palindrome exercise.
     */
    public static void palindromeExecution() {
        final ArrayList<Integer> valuesList = new ArrayList<>();

        int testCases = UtilExercises.validateRange(MINIMUM_VALUE_TEST_CASE, MAXIMUM_VALUE_TEST_CASE, Integer.parseInt(UtilExercises.requestEntryValue()));

        while (testCases > 0) {
            final int value2 = UtilExercises.validateRange(INPUT_MINIMUM_VALUE, INPUT_MAXIMUM_VALUE, Integer.parseInt(UtilExercises.requestEntryValue()));
            valuesList.add(value2);
            testCases--;
        }
        //Iteration for each case.
        for (int iterator : valuesList) {
            System.out.println(findLargestPalindrome(iterator));
        }


    }
}
