/*
 *      File: UtilExercises.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
package com.erick.mx.utility;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Description.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class UtilExercises {

    /**
     * Class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UtilExercises.class);

    /**
     * Scanner class for key inputs.
     */
    private static final Scanner SCANNER = new Scanner(System.in);


    /**
     * Method to request a parameter by keyboard.
     * <p>
     * <p>This method asks for a value per keyboard and validates if it is a numeric type, if not, the user has two opportunities to perform
     * the capture correctly.</p>
     *
     * @return The captured parameter or an exception if the opportunities are finished.
     */
    public static String requestEntryValue() {

        final int count = 0;
        int maxTries = 2;
        while (true) {
            try {
                String input = SCANNER.nextLine();
                Integer.parseInt(input);
                return input;
            } catch (NumberFormatException e) {
                LOGGER.error(String.format("You can only enter numeric values, you have left: %d opportunities", maxTries));
                if (count == maxTries--) {
                    throw e;
                }
            }

        }
    }

    /**
     * Method to validate the range of a value.
     *
     * @param min   The minimum allowed value.
     * @param max   The maximum allowed value.
     * @param value The value to The value to evaluate.
     * @return The value or an exception if the range is broken.
     */
    public static int validateRange(int min, int max, int value) {
        if (value < min || value > max) {
            throw new IllegalArgumentException("The values is bigger than " + max + " or smaller than " + min);
        }
        return value;
    }

    /**
     * Validate the input for product exercise.
     *
     * @param consecutiveDigits The number of consecutive digits allowed.
     * @return The input received.
     */
    public static String validateProductInput(int consecutiveDigits) {
        String input = SCANNER.nextLine();
        if (input.matches("[0-9]+") && input.length() == consecutiveDigits) {
            return input;
        }
        throw new IllegalArgumentException(String.format("Your entry contains more than: %d digits or invalid characters", consecutiveDigits));
    }

}
