/*
 *      File: package-info.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
/**
 * This package contains all the utility classes.
 */
package com.erick.mx.utility;