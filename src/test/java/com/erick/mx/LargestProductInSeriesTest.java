/*
 *      File: LargestProductInSeriesTest.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: oct 08, 2018
 */
package com.erick.mx;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for largest product in series.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestProductInSeriesTest {

    @Test
    public void findLargesProduct() throws Exception {

        Assert.assertEquals("1260", LargestProductInSeries.findLargesProduct("7652342342", 5));
    }

}