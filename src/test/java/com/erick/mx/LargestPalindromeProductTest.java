/*
 *      File: LargestPalindromeProductTest.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: oct 08, 2018
 */
package com.erick.mx;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for largest palindrome product
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestPalindromeProductTest {

    @Test
    public void isPalindromeTrue() throws Exception {
        Assert.assertTrue(LargestPalindromeProduct.isPalindrome(1001));
    }

    @Test
    public void isPalindromeFalse() throws Exception {
        Assert.assertFalse(LargestPalindromeProduct.isPalindrome(1201));
    }

    @Test
    public void findLargestPalindrome() throws Exception {
        long value =LargestPalindromeProduct.findLargestPalindrome(101110L);
        Assert.assertEquals(value,101101);
    }

    @Test
    public void findLargestPalindromeNegativeValue() throws Exception {
        long value =LargestPalindromeProduct.findLargestPalindrome(1L);
        Assert.assertEquals(value,-1);
    }

}