/*
 *      File: LargestProductInputTest.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 08, 2018
 */
package com.erick.mx.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for largest product input
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class LargestProductInputTest {

    private LargestProductInput largestProductInput = new LargestProductInput("3675356291", 7);
    private LargestProductInput largestProductInputEquals = new LargestProductInput("3675356291", 7);
    private LargestProductInput largestProductInputDiff = new LargestProductInput("36753", 7);
    private LargestProductInput largestProductInputLimitDiff = new LargestProductInput("36753", 6);


    @Test
    public void checkConsistency() {
        Assert.assertEquals(largestProductInput, largestProductInput);
        Assert.assertEquals(largestProductInput, largestProductInputEquals);
        Assert.assertNotEquals(largestProductInput, null);
        Assert.assertNotEquals(largestProductInput, largestProductInputDiff);
        Assert.assertNotEquals(largestProductInput, largestProductInputLimitDiff);
    }

}